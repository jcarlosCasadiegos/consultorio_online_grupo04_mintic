package Model.clases;

//import java.sql.Date;
import java.util.Date;

public class Cita {
    
    //Atributos
    private int id;
    private String tipoCita;
    private Date fechaRegistro;
    private Date fechaCita;
    private String pacienteCedula;

    public Cita(int id, String tipoCita, Date fechaRegistro, Date fechaCita, String pacienteCedula) {
        this.id = id;
        this.tipoCita = tipoCita;
        this.fechaRegistro = fechaRegistro;
        this.fechaCita = fechaCita;
        this.pacienteCedula = pacienteCedula;
    }
    
    public Cita(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipoCita() {
        return tipoCita;
    }

    public void setTipoCita(String tipoCita) {
        this.tipoCita = tipoCita;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(Date fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getPacienteCedula() {
        return pacienteCedula;
    }

    public void setPacienteCedula(String pacienteCedula) {
        this.pacienteCedula = pacienteCedula;
    }

    @Override
    public String toString() {
        return "Cita{" + "id=" + id + ", tipoCita=" + tipoCita + ", fechaRegistro=" + fechaRegistro + ", fechaCita=" + fechaCita + ", pacienteCedula=" + pacienteCedula + '}';
    }
    
    
    
}
