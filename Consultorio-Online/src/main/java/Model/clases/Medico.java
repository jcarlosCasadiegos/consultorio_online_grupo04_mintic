package Model.clases;

public class Medico extends Persona{
    
    //Atributos d ela clase.
    private String profesion;
    private double salario;


    public Medico(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion, String usuario, String contrasenia, String profesion, double salario) {
        super(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia);
        this.profesion = profesion;
        this.salario = salario;
    }
    
    public Medico(){}    

    public Medico(String profesion, double salario) {
        this.profesion = profesion;
        this.salario = salario;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    
    

    @Override
    public String toString() {
        return "Medico{"+ "cedula=" + super.getCedula() + ", nombre=" + super.getNombre() + ", apellido=" + super.getApellido() 
                + ", edad=" + super.getEdad() + ", telefono=" + super.getTelefono() + ", correo=" + super.getCorreo() + ", direccion=" + super.getDireccion()
                + ", usuario=" + super.getUsuario() + ", contrasenia=" + super.getContrasenia() + ", profesion=" + profesion + ", salario=" + salario + '}';
    }

    
    
    
}
