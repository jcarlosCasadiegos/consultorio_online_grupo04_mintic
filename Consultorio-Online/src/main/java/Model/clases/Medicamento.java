package Model.clases;

public class Medicamento {
    
    private int id;
    private String nombre;
    private String presentacion;
    private String descripcion;
    private String fabricante;

    public Medicamento(int id, String nombre, String presentacion, String descripcion, String fabricante) {
        this.id = id;
        this.nombre = nombre;
        this.presentacion = presentacion;
        this.descripcion = descripcion;
        this.fabricante = fabricante;
    }
    
    public Medicamento(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    @Override
    public String toString() {
        return "Medicamento{" + "id=" + id + ", nombre=" + nombre + ", presentacion=" + presentacion + ", descripcion=" + descripcion + ", fabricante=" + fabricante + '}';
    }
    
    public void contarVentas(){
        
    }
}
