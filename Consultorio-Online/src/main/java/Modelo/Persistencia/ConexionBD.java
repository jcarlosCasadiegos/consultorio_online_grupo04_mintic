package Modelo.Persistencia;

import com.mysql.cj.jdbc.DatabaseMetaData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class ConexionBD {
    
    private static String url = "";
    private static String user = "root";
    private static String pass = "carlos1997";
    public static Connection connection = null;
    
    public static Connection iniciar_conexion() throws ClassNotFoundException, SQLException{
        url = "jdbc:mysql://localhost:3306/consultorio";
        Class.forName("com.mysql.cj.jdbc.Driver");
        try{
            connection = DriverManager.getConnection(url, user, pass);
            if(connection != null){
                DatabaseMetaData meta =  (DatabaseMetaData) connection.getMetaData();
                System.out.println("Base de datos conectada. " + meta.getDriverName());
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return connection;        
    }
}
