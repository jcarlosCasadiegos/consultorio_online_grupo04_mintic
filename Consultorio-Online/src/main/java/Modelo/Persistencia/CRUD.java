package Modelo.Persistencia;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class CRUD {
    
    public static Connection connection;
    public static Statement statement = null;
    public static ResultSet resultSet = null;
    
    //Asignar la conexión de la base de datos.
    public static Connection set_connection(Connection connection){
        CRUD.connection = connection;
        return connection;
    }
    
    //Retornar la conexión.
    public static Connection get_connection(){
        return connection;
    }
    
    //Cerrar conexión.
    public static void close_connetion(Connection connection){
        if(connection != null){
            try{
                connection.close();
            }
            catch(SQLException e){
                Logger.getLogger(CRUD.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
    
    //Insertar a la base de datos.
    public static boolean insertarBD(String sentencia){
        try{
            statement = connection.createStatement();
            statement.execute(sentencia);
        }
        catch(SQLException | RuntimeException ex){
            System.out.println("ERRORc RUTINA " + ex);
            return false;
        }
        return true;
    }
    
    //Consultar de la base de datos.
    public static ResultSet consultarBD(String sentencia){
        try{
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sentencia);
        }
        catch(SQLException se){
            System.out.println(se.getMessage());
        }
        catch(RuntimeException re){
            System.out.println(re.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return resultSet;
    }
    
    //Borrar de la base de datos.
    public static boolean borrarBD(String sentencia){
        try{
            statement = connection.createStatement();
            statement.execute(sentencia);
        }
        catch(SQLException | RuntimeException ex){
            System.out.println("ERROR RUTINA " + ex);
            return false;
        }
        return true;
    }
    
    //Actualizar de la base de datos.
    public static boolean actualizarBD(String sentencia){
        try{
            statement = connection.createStatement();
            statement.execute(sentencia);
        }
        catch(SQLException | RuntimeException ex){
            System.out.println("ERROR RUTINA " + ex);
            return false;
        }
        return true;
    }
        
    
    //Auto commit
    public static boolean set_auto_commit(boolean parametro){
        try{
            connection.setAutoCommit(parametro);
        }
        catch(SQLException e){
            System.out.println("Error al configurar el autoCommit " + e.getMessage());
            return false;
        }
        return true;
    }
    
    //Commit
    public static boolean commitBD(){
        try{
            connection.commit();
            return true;
        }
        catch(SQLException e){
            System.out.println("Error al hacer commit " + e.getMessage());
            return false;
        }
    }
    
    //RollBack
    public static boolean roll_back(){
        try{
            connection.rollback();
            return true;
        }
        catch(SQLException e){
            System.out.println("Error al hacer roll back " + e.getMessage());
            return false;
        }
    }
    
    //Cerrar conexión.
    public static void cerrar_conexion(){
        close_connetion(connection);
    }
    
}
