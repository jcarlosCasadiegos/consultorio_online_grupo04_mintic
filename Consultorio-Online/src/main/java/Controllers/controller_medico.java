package Controllers;

import Modelo.Persistencia.CRUD;
import Modelo.Persistencia.ConexionBD;
import Model.clases.Persona;
import Model.clases.Medico;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class controller_medico {
    
    //Metodo de registrar medicos en la base de datos funcionando perfectamente.
    
    //Registrar.
    public static boolean registrarMedico(String cedula, String nombre, String apellido,  int edad, String telefono, String correo, String direccion, String usuario, String contrasenia, String profesion, double salario) throws ClassNotFoundException, SQLException{
        Medico m = new Medico(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia, profesion, salario); 
        
        //Iniciamos la conexión de la base de datos.
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        
        //Realizamos en insert a la base de datos.
        String sql = "INSERT INTO medicos(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia, profesion, salario) "
                + "VALUES('" + m.getCedula() + "','" + m.getNombre() + "','" + m.getApellido() + "','" + m.getEdad() + "','" + m.getTelefono() + "','" + m.getCorreo() + "','" + m.getDireccion() + "','" + m.getUsuario() + "','" + m.getContrasenia() + "','" + m.getProfesion() + "','" + m.getSalario() + "');";                                                              
        
        if(CRUD.set_auto_commit(false)){
            if(CRUD.insertarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    //Actualizar médico funcionando al 100%.    
    //Actualizar en la base de datos.
    public static boolean actualizarMedico(String cedula, String nombre, String apellido,  int edad, String telefono, String correo, String direccion, String usuario, String contrasenia, String profesion, double salario) throws ClassNotFoundException, SQLException{
        Medico m = new Medico(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia, profesion, salario);
        
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        
        String sql = "UPDATE medicos SET cedula='" + m.getCedula() + "', nombre='" + m.getNombre() + "', apellido='" + m.getApellido() + "', edad='" + m.getEdad() 
                + "', telefono='" + m.getTelefono() + "', correo='" + m.getCorreo() + "', direccion='" + m.getDireccion() + "', usuario='" + m.getUsuario() 
                + "', contrasenia='" + m.getContrasenia() + "', profesion='" + m.getProfesion() + "', salario='" + m.getSalario() + "' " 
                + "WHERE cedula='" + m.getCedula() + "';";                      
        
        if(CRUD.set_auto_commit(false)){
            if(CRUD.actualizarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    
    //Metodo eliminar funcionando al 100%.
    
    //Eliminar medico de la base de datos.
    public static boolean eliminarMedico(String cedula) throws ClassNotFoundException, SQLException{
        
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        
        String sql = "DELETE FROM medicos WHERE cedula='" + cedula + "';";
        if(CRUD.set_auto_commit(false)){
            if(CRUD.borrarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        CRUD.cerrar_conexion();
        return false;
    }
    
    //Funcionando al 100%
    //Consultar un medico de la base de datos.
    public static Medico consultarMedico(String cedula) throws ClassNotFoundException, SQLException{
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "SELECT *FROM medicos WHERE cedula='" + cedula + "';";
        
        ResultSet rs = CRUD.consultarBD(sql);
        Medico medico = new Medico();
        
        try{
            if(rs.next()){
                medico.setCedula(rs.getString("cedula"));
                medico.setNombre(rs.getString("nombre"));
                medico.setApellido(rs.getString("apellido"));
                medico.setEdad(rs.getInt("edad"));
                medico.setTelefono(rs.getString("telefono"));
                medico.setCorreo(rs.getString("telefono"));
                medico.setDireccion(rs.getString("direccion"));
                medico.setUsuario(rs.getString("usuario"));
                medico.setContrasenia(rs.getString("contrasenia"));
                medico.setProfesion(rs.getString("profesion"));
                medico.setSalario(rs.getDouble("salario"));                
            }
            else{
                CRUD.cerrar_conexion();
                return null;
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
        return medico;
    }
    
    //Funcionando al 100%
    //Consultar todos los registros de la base de datos.    
    public static List<Medico> consultarTodosMedicos() throws ClassNotFoundException, SQLException{
        
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        List<Medico> listaMedico = new ArrayList<>();
        
        try{
            String sql = "SELECT *FROM medicos";
            
            ResultSet rs = CRUD.consultarBD(sql);
            
            while(rs.next()){
                Medico medico = new Medico();
                medico.setCedula(rs.getString("cedula"));
                medico.setNombre(rs.getString("nombre"));
                medico.setApellido(rs.getString("apellido"));
                medico.setEdad(rs.getInt("edad"));
                medico.setTelefono(rs.getString("telefono"));
                medico.setCorreo(rs.getString("correo"));
                medico.setDireccion(rs.getString("direccion"));
                medico.setUsuario(rs.getString("usuario"));
                medico.setContrasenia(rs.getString("contrasenia"));
                medico.setProfesion(rs.getString("profesion"));
                medico.setSalario(rs.getDouble("salario"));
                
                listaMedico.add(medico);
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        finally{
            CRUD.cerrar_conexion();
        }
        
        return listaMedico;
    }
    
}
