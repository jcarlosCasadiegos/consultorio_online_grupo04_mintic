package Controllers;

import Model.clases.Paciente;
import Modelo.Persistencia.CRUD;
import Modelo.Persistencia.ConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class controller_paciente {
 
    //Metodo funcionando correctamente.
    //Registrar paciente.
    public static boolean registrarPaciente(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion, String usuario, String contrasenia) throws ClassNotFoundException, SQLException{
        Paciente p = new Paciente(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia);
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "INSERT INTO pacientes(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia)"
            + "VALUES('" + p.getCedula() + "','" + p.getNombre() + "','" + p.getApellido() + "','" + p.getEdad() + "','" + p.getTelefono() + "','" + p.getCorreo()
                + "','" + p.getDireccion() + "','" + p.getUsuario() + "','" + p.getContrasenia() + "');";
        
        if(CRUD.set_auto_commit(false)){
            if(CRUD.insertarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    //Metodo funcionando correctamente
    //Metodo actualizar paciente.
    public static boolean actualizarPaciente(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion, String usuario, String contrasenia) throws ClassNotFoundException, SQLException{
        
        Paciente p = new Paciente(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia);
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "UPDATE pacientes SET cedula='" + p.getCedula() + "', nombre='" + p.getNombre() + "', apellido='" + p.getApellido() + "', edad='" + p.getEdad()
                + "', telefono='" + p.getTelefono() + "', correo='" + p.getCorreo() + "', direccion='" + p.getDireccion() + "', usuario='" + p.getUsuario()
                + "', contrasenia='" + p.getContrasenia() + "' WHERE cedula='" + p.getCedula() + "';";
        
        if(CRUD.set_auto_commit(false)){
            if(CRUD.actualizarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    //Debe existir otro modificar paciente donde se deje modificar la cedula. Parte del administrador.
    
    
    
    //Metodo funcionando.
    //Metodo eliminar paciente.
    public static boolean eliminarPaciente(String cedula, String nombre, String apellido, int edad, String telefono, String correo, 
            String direccion, String usuario, String contrasenia) throws ClassNotFoundException, SQLException{
        
        Paciente paciente = new Paciente(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia);
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "DELETE pacientes WHERE cedula='" + paciente.getCedula() + "';";
        if(CRUD.set_auto_commit(false)){
            if(CRUD.borrarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    //Funcioanando sin errores
    //Consultar un paciente.
    public static Paciente consultarPaciente(String cedula) throws ClassNotFoundException, SQLException{
        
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "SELECT *FROM pacientes WHERE cedula='" + cedula + "';";
        ResultSet rs = CRUD.consultarBD(sql);
        Paciente p = new Paciente();
        try{
            if(rs.next()){
                p.setCedula(rs.getString("cedula"));
                p.setNombre(rs.getString("nombre"));
                p.setApellido(rs.getString("apellido"));
                p.setEdad(rs.getInt("edad"));
                p.setTelefono(rs.getString("telefono"));
                p.setCorreo(rs.getString("correo"));
                p.setDireccion(rs.getString("direccion"));
                CRUD.cerrar_conexion();
            }
            else{
                CRUD.cerrar_conexion();
                return null;
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
        return p;
    }
    
    //Funcionando sin errores.
    //Consultar todos los pacientes.
    public static List<Paciente> listarTodosPacientes() throws ClassNotFoundException, SQLException{
        
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        
        List<Paciente> listaPacientes = new ArrayList<>();
        try{
            String sql = "SELECT *FROM pacientes";
            ResultSet rs = CRUD.consultarBD(sql);
            
            while(rs.next()){
                Paciente paciente = new Paciente();
                
                paciente.setCedula(rs.getString("cedula"));
                paciente.setNombre(rs.getString("nombre"));
                paciente.setApellido(rs.getString("apellido"));
                paciente.setEdad(rs.getInt("edad"));
                paciente.setTelefono(rs.getString("telefono"));
                paciente.setCedula(rs.getString("correo"));
                paciente.setDireccion(rs.getString("direccion"));
                listaPacientes.add(paciente);
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        finally{
            CRUD.cerrar_conexion();
        }
        return listaPacientes;
    }
    
}
