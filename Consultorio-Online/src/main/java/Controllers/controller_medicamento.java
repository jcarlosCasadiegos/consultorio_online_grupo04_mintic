package Controllers;

import Model.clases.Medicamento;
import Modelo.Persistencia.CRUD;
import Modelo.Persistencia.ConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class controller_medicamento {
    
    //Funcionando sin errores.
    //registrar medicamento
    public static boolean registrarMedicamento(String nombre, String presentacion, String descripcion, String fabricante) throws ClassNotFoundException, SQLException{
        Medicamento medi = new Medicamento(0, nombre, presentacion, descripcion, fabricante);
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "INSERT INTO medicamentos(nombre, presentacion, descripcion, fabricante) "
                + "VALUES('" + medi.getNombre() + "','" + medi.getPresentacion() + "','" + medi.getDescripcion() + "','" + medi.getFabricante() + "');";
        if(CRUD.set_auto_commit(false)){
            if(CRUD.insertarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    //Actualizar medicamentos.
    public static boolean actualizarMedicamento(int id, String nombre, String presentacion, String descripcion, String fabricante) throws ClassNotFoundException, SQLException{
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        Medicamento medi = new Medicamento(id, nombre, presentacion, descripcion, fabricante);
        String sql = "UPDATE medicamentos SET nombre='" + medi.getNombre() + "', presentacion='" + medi.getPresentacion() + "', descripcion='" + medi.getDescripcion() +
                "', fabricante='" + medi.getFabricante() + "' WHERE id='" + medi.getId() + "';";
        if(CRUD.set_auto_commit(false)){
            if(CRUD.actualizarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    //Consultar medicamento
    public static Medicamento consultarMedicamento(String id) throws ClassNotFoundException, SQLException{
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "SELECT *FROM medicamentos WHERE id='" + id + "';";
        
        ResultSet rs = CRUD.consultarBD(sql);
        Medicamento medi = new Medicamento();
        
        try{
            if(rs.next()){
                medi.setId(rs.getInt("id"));
                medi.setNombre(rs.getString("nombre"));
                medi.setPresentacion(rs.getString("presentacion"));
                medi.setDescripcion(rs.getString("descripcion"));
                medi.setFabricante(rs.getString("fabricante"));               
            }
            else{
                CRUD.cerrar_conexion();
                return null;
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
        return medi;
    }
    
    //Consultar todos los medicamentos
    public static List<Medicamento> consultarTodosMedicamento() throws ClassNotFoundException, SQLException{
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        List<Medicamento> listaMedicamentos = new ArrayList<>();
                
        try{
            String sql = "SELECT *FROM medicamentos";
            ResultSet rs = CRUD.consultarBD(sql);
            
            while(rs.next()){
                Medicamento medi = new Medicamento();
                medi.setId(rs.getInt("id"));
                medi.setNombre(rs.getString("nombre"));
                medi.setPresentacion(rs.getString("presentacion"));
                medi.setDescripcion(rs.getString("descripcion"));
                medi.setFabricante(rs.getString("fabricante")); 
                listaMedicamentos.add(medi);
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        finally{
            CRUD.cerrar_conexion();
        }
        return listaMedicamentos;
    }
    
}
