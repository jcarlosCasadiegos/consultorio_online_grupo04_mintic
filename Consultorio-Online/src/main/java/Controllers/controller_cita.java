package Controllers;

import Model.clases.Cita;
import Modelo.Persistencia.CRUD;
import Modelo.Persistencia.ConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class controller_cita {
    
    //Registrar citas.
    public static boolean registrarCitas(String tipoCita, Date fechaRegistro, Date fechaCita, String pacienteCedula) throws ClassNotFoundException, SQLException{
        Cita cita = new Cita(0, tipoCita, fechaRegistro, fechaCita, pacienteCedula);
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "INSERT INTO citas(tipoCita, fechaRegistro, fechaCita, pacienteCedula)"
                + " VALUES('" + cita.getTipoCita() + "','" + cita.getFechaRegistro() + "','" + cita.getFechaCita() + "','" + cita.getPacienteCedula() + "');";
        if(CRUD.set_auto_commit(false)){
            if(CRUD.insertarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    //Actualizar cita: Solo se puede actualizar la cita si está a mas de dos días antes de la fecha estipulada de la cita.
    
    //Eliminar cita: Solo se pueden eliminar citas si el usuario que las está eliminando es el administrador.
    
    //Falta llamar el tipo de fecha correcto.
    //Consultar cita
    public static Cita consultarCita(String id) throws ClassNotFoundException, SQLException{
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "SELECT *FROM citas WHERE id='" + id +"';";
        
        ResultSet rs = CRUD.consultarBD(sql);
        Cita cita = new Cita();
        try{
            if(rs.next()){
                cita.setId(rs.getInt("id"));
                cita.setTipoCita(rs.getString("tipoCita"));
                cita.setFechaRegistro(rs.getDate("fechaRegistro"));
                cita.setFechaCita(rs.getDate("fechaCita"));
                cita.setPacienteCedula(rs.getString("pacienteCedula"));
                CRUD.cerrar_conexion();
            }
            else{
                CRUD.cerrar_conexion();
                return null;
            }
        }
        catch(SQLException e){
            System.out.println("Datos de la cita no encontrado " + e.getMessage());
            return null;
        }
        return cita;
    }
    
    //Consultar todas las citas, Como administrador.
    public static List<Cita> consultarTodasCitas() throws ClassNotFoundException, SQLException{
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        
        String sql = "SELECT *FROM citas";
        ResultSet rs = CRUD.consultarBD(sql);
        List<Cita> listaCitas = new ArrayList<>();
        
        try{
            while(rs.next()){
                Cita cita = new Cita();
                cita.setId(rs.getInt("id"));
                cita.setTipoCita(rs.getString("tipoCita"));
                cita.setFechaRegistro(rs.getDate("fechaRegistro"));
                cita.setFechaCita(rs.getDate("fechaCita"));
                cita.setPacienteCedula(rs.getString("pacienteCedula"));
                listaCitas.add(cita);
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        finally{
            CRUD.cerrar_conexion();
        }
        return listaCitas;
    }
    
}
