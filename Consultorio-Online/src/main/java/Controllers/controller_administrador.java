package Controllers;

import Model.clases.Administrador;
import Modelo.Persistencia.CRUD;
import Modelo.Persistencia.ConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;

public class controller_administrador {
    
    //Registra los usuario correctamente.
    //Registrar administradores.
    public static boolean registrarAdministrador(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion,
            String usuario, String contrasenia) throws ClassNotFoundException, SQLException{
        
        Administrador admin = new Administrador(cedula, nombre, apellido, edad, telefono, correo,direccion, usuario, contrasenia);
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "INSERT INTO administradores(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia)"
                + "VALUES('" + admin.getCedula() + "','" + admin.getNombre() + "','" + admin.getApellido() + "','" + admin.getEdad() + "','" +
                admin.getTelefono() + "','" + admin.getCorreo() + "','" + admin.getDireccion() + "','" + admin.getUsuario() + "','" + 
                admin.getContrasenia() + "');";
        
        if(CRUD.set_auto_commit(false)){
            if(CRUD.insertarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    //Metodo funcionando correctamente.
    //Actualizar administrador.
    public static boolean actualizarAdministrador(String cedula, String nombre, String apellido, int edad, String telefono, String correo, 
            String direccion, String usuario, String contrasenia) throws ClassNotFoundException, SQLException{
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        
        Administrador admin = new Administrador(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia);
        String sql = "UPDATE administradores SET cedula='"+ admin.getCedula() + "', nombre='" + admin.getNombre() + "', apellido='" + admin.getApellido() +
                "', edad='" + admin.getEdad() + "', telefono='" + admin.getTelefono() + "', correo='" + admin.getCorreo() + "', direccion='" + admin.getDireccion() +
                "', usuario='" + admin.getUsuario() + "', contrasenia='" + admin.getContrasenia() + ";";
        
        if(CRUD.set_auto_commit(false)){
            if(CRUD.actualizarBD(sql)){
                CRUD.commitBD();
                CRUD.cerrar_conexion();
                return true;
            }
            else{
                CRUD.roll_back();
                CRUD.cerrar_conexion();
                return false;
            }
        }
        else{
            CRUD.cerrar_conexion();
            return false;
        }
    }
    
    public static Administrador consultarAdministrador(String cedula) throws ClassNotFoundException, SQLException{
        CRUD.set_connection(ConexionBD.iniciar_conexion());
        String sql = "SELECT *FROM administradores WHERE cedula='" + cedula + "';";
        
        ResultSet rs = CRUD.consultarBD(sql);
        Administrador admin = new Administrador();
        try{
            if(rs.next()){
                admin.setCedula(rs.getString("cedula"));
                admin.setNombre(rs.getString("nombre"));
                admin.setApellido(rs.getString("apellido"));
                admin.setEdad(rs.getInt("edad"));
                admin.setTelefono(rs.getString("telefono"));
                admin.setCorreo(rs.getString("correo"));
                admin.setDireccion(rs.getString("direccion"));
                admin.setUsuario(rs.getString("usuario"));
                admin.setContrasenia(rs.getString("contrasenia"));
            
                CRUD.cerrar_conexion();
            }
            else{
                CRUD.cerrar_conexion();
                return null;
            }
        }
        catch(SQLException e){
            System.out.println("Error al obtener los datos del administrador " + e.getMessage());
            return null;
        }
        return admin;
    }
}
